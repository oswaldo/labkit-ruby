# frozen_string_literal: true

require_relative "./grpc_service/test_pb"
require_relative "./grpc_service/test_service_impl"
require_relative "./grpc_service/test_service_mock"
