# frozen_string_literal: true

describe Labkit::Tracing::Sidekiq::ServerMiddleware do
  describe "#call" do
    let(:worker_class) { "test_worker_class" }
    let(:job) { { "class" => "jobclass", "queue" => "jobqueue", "retry" => 0, "args" => %w[1 2 3] } }
    let(:queue) { "test_queue" }
    let(:custom_error) { Class.new(StandardError) }
    let(:span) { OpenTracing.start_span("test", ignore_active_scope: true) }

    subject { described_class.new }

    it "yields" do
      expect(Labkit::Tracing::TracingUtils).to receive(:with_tracing)
                                                 .with(
                                                   hash_including(
                                                     operation_name: "sidekiq:jobclass",
                                                     tags: {
                                                       "component" => "sidekiq",
                                                       "span.kind" => "server",
                                                       "sidekiq.queue" => "jobqueue",
                                                       "sidekiq.jid" => nil,
                                                       "sidekiq.retry" => "0",
                                                       "sidekiq.args" => "1, 2, 3",
                                                     },
                                                   ),
                                                 ).and_yield(span)

      expect { |b| subject.call(worker_class, job, queue, &b) }.to yield_control
    end

    it "propagates exceptions" do
      expect { subject.call(worker_class, job, queue) { raise custom_error } }.to raise_error(custom_error)
    end
  end
end
