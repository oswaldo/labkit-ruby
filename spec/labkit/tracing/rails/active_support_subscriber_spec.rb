# frozen_string_literal: true

describe Labkit::Tracing::Rails::ActiveSupportSubscriber do
  using RSpec::Parameterized::TableSyntax

  describe ".instrument" do
    it "is unsubscribeable" do
      unsubscribe = described_class.instrument

      expect(unsubscribe).not_to be_nil
      expect { unsubscribe.call }.not_to raise_error
    end
  end

  describe "#notify_cache_read" do
    subject { described_class.new }

    let(:start) { Time.now }
    let(:finish) { Time.now }

    where(:key, :hit, :super_operation) do
      nil | nil | nil
      123 | nil | nil
      123 | true | nil
      123 | false | nil
      123 | true | "fetch"
    end

    with_them do
      def payload
        { key: key, hit: hit, super_operation: super_operation }
      end

      def expected_tags
        {
          "component" => "ActiveSupport",
          "cache.key" => key,
          "cache.hit" => hit,
          "cache.super_operation" => super_operation,
        }
      end

      it "should notify the tracer when the hash contains null values" do
        expect(Labkit::Tracing::TracingUtils).to receive(:postnotify_span).with("cache_read", start, finish, tags: expected_tags, exception: nil)

        subject.notify_cache_read(start, finish, payload)
      end

      it "should notify the tracer when the payload is missing values" do
        expect(Labkit::Tracing::TracingUtils).to receive(:postnotify_span).with("cache_read", start, finish, tags: expected_tags, exception: nil)

        subject.notify_cache_read(start, finish, payload.compact)
      end

      it "should not throw exceptions when with the default tracer" do
        expect { subject.notify_cache_read(start, finish, payload) }.not_to raise_error
      end
    end
  end

  describe "#notify_*" do
    subject { described_class.new }

    let(:start) { Time.now }
    let(:finish) { Time.now }

    where(:method, :operation_name, :key) do
      :notify_cache_generate | "cache_generate" | nil
      :notify_cache_generate | "cache_generate" | 123
      :notify_cache_fetch_hit | "cache_fetch_hit" | nil
      :notify_cache_fetch_hit | "cache_fetch_hit" | 123
      :notify_cache_write | "cache_write" | nil
      :notify_cache_write | "cache_write" | 123
      :notify_cache_delete | "cache_delete" | nil
      :notify_cache_delete | "cache_delete" | 123
    end

    with_them do
      def payload
        { key: key }
      end

      def expected_tags
        {
          "component" => "ActiveSupport",
          "cache.key" => key,
        }
      end

      it "should notify the tracer when the hash contains null values" do
        expect(Labkit::Tracing::TracingUtils).to receive(:postnotify_span).with(operation_name, start, finish, tags: expected_tags, exception: nil)

        subject.method(method).call(start, finish, payload)
      end

      it "should notify the tracer when the payload is missing values" do
        expect(Labkit::Tracing::TracingUtils).to receive(:postnotify_span).with(operation_name, start, finish, tags: expected_tags, exception: nil)

        subject.method(method).call(start, finish, payload.compact)
      end

      it "should not throw exceptions when with the default tracer" do
        expect { subject.method(method).call(start, finish, payload) }.not_to raise_error
      end
    end
  end
end
