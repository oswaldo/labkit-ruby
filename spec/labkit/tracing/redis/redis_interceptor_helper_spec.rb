# frozen_string_literal: true

describe Labkit::Tracing::Redis::RedisInterceptorHelper do
  using RSpec::Parameterized::TableSyntax

  describe ".common_tags_for_client" do
    where(:scheme, :host, :port, :path) do
      nil | nil | nil | nil
      "redis" | "localhost" | 6379 | "/"
      "redis-sentinel" | "localhost" | 6379 | "/"
    end

    with_them do
      def client
        instance_double("client", :scheme => scheme, :host => host, :port => port, :path => path)
      end

      def expected_tags
        {
          "component" => "redis",
          "span.kind" => "client",
          "redis.scheme" => scheme,
          "redis.host" => host,
          "redis.port" => port,
          "redis.path" => path,
        }
      end

      it "should generate the correct tags" do
        expect(described_class.common_tags_for_client(client)).to eq(expected_tags)
      end
    end
  end

  describe ".tags_from_command" do
    where(:command, :command_tag) do
      nil | ""
      [] | ""
      ["FLUSHALL"] | "FLUSHALL"
      %w(AUTH secret) | "AUTH *****"
      [:auth, "secret"] | "auth *****"
      %w(GET key) | "GET key"
      %w(GET a:b) | "GET a:b"
      %w(GET a:b:c) | "GET a:b:c"
      %w(GET a:b:c:d) | "GET a:b:c:*****"
      %w(GET users/key-count-service/12345) | "GET users/key-count-service/12345"
      %w(GET project_#123_metrics_dashboard_/dashboards) | "GET project_#123_metrics_dashboard_/dashboards"
      %w(GET project_#123_metrics_dashboard_/dashboards/123) | "GET project_#123_metrics_dashboard_/dashboards/*****"
      %w(MGET 1 2 3) | "MGET 1 ...2 more value(s)"
      [nil, "value"] | "nil value"
      nil | ""
      false | ""
      [:get, 1] | "get 1"
      [:get, 1.0] | "get 1.0"
      [:get, 1, "two"] | "get 1 ...1 more value(s)"
    end

    with_them do
      def client
        instance_double("client", :scheme => "redis", :host => "localhost", :port => 6379, :path => "/")
      end

      def expected_tags
        {
          "component" => "redis",
          "span.kind" => "client",
          "redis.scheme" => "redis",
          "redis.host" => "localhost",
          "redis.port" => 6379,
          "redis.path" => "/",
          "redis.command" => command_tag,
        }
      end

      it "should generate the correct tags" do
        expect(described_class.tags_from_command(command, client)).to eq(expected_tags)
      end
    end

    describe ".tags_from_pipeline" do
      where(:commands, :commands_tags) do
        [] | []
        [%w(GET moo)] | ["GET moo"]
        [%w(GET moo), %w(SET moo)] | ["GET moo", "SET moo"]
        [%w(GET a:b:c), %w(SET a:b:c:d)] | ["GET a:b:c", "SET a:b:c:*****"]
      end

      with_them do
        def client
          instance_double("client", :scheme => "redis", :host => "localhost", :port => 6379, :path => "/")
        end

        def pipeline
          instance_double("pipeline", :commands => commands)
        end

        def expected_tags
          tags = {
            "component" => "redis",
            "span.kind" => "client",
            "redis.scheme" => "redis",
            "redis.host" => "localhost",
            "redis.port" => 6379,
            "redis.path" => "/",
            "redis.pipeline.commands.length" => commands_tags.length,
          }

          commands_tags.each_with_index do |command, index|
            tags["redis.command.#{index}"] = command
          end

          tags
        end

        it "should generate the correct tags" do
          expect(described_class.tags_from_pipeline(pipeline, client)).to eq(expected_tags)
        end
      end
    end
  end
end
