# frozen_string_literal: true

require "./spec/support/grpc_service"

describe Labkit::Correlation::GRPC::ServerInterceptor do
  describe "running on RpcServer" do
    subject { described_class.new }

    let(:mock_server) { LabkitTest::TestService::MockServer.new }
    let(:client) { mock_server.start(server_interceptors: [subject]) }

    after do
      mock_server.stop
    end

    shared_examples_for "a server interceptor" do
      def metadata_with_correlation_id
        tags = {}
        tags[Labkit::Correlation::GRPC::GRPCCommon::CORRELATION_METADATA_KEY] = correlation_id if correlation_id

        tags
      end

      describe "#req_res_method" do
        it "generates instrumentation" do
          expect(Labkit::Correlation::CorrelationId).to receive(:use_id).with(expected_correlation_id).and_call_original

          client.req_res_method(LabkitTest::Msg.new, metadata: metadata_with_correlation_id)
        end
      end

      describe "#server_stream_method" do
        it "generates instrumentation" do
          expect(Labkit::Correlation::CorrelationId).to receive(:use_id).with(expected_correlation_id).and_call_original

          enumerator = client.server_stream_method(LabkitTest::Msg.new, metadata: metadata_with_correlation_id)
          enumerator.each { } # Consume the stream
        end
      end

      describe "#client_stream_method" do
        it "generates instrumentation" do
          expect(Labkit::Correlation::CorrelationId).to receive(:use_id).with(expected_correlation_id).and_call_original

          client.client_stream_method([LabkitTest::Msg.new], metadata: metadata_with_correlation_id)
        end
      end

      describe "#bidi_stream_method" do
        it "generates instrumentation" do
          expect(Labkit::Correlation::CorrelationId).to receive(:use_id).with(expected_correlation_id).and_call_original

          enumerator = client.bidi_stream_method([LabkitTest::Msg.new], metadata: metadata_with_correlation_id)
          enumerator.each { |x| } # Consume the stream
        end
      end
    end

    describe "with correlation_id" do
      let(:correlation_id) { "12345" }
      let(:expected_correlation_id) { correlation_id }

      it_behaves_like "a server interceptor"
    end

    describe "without correlation_id" do
      let(:correlation_id) { nil }
      let(:expected_correlation_id) { anything }

      it_behaves_like "a server interceptor"
    end
  end
end
